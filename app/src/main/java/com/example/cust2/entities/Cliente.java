package com.example.cust2.entities;

import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity (indices = {@Index(value = {"nombre"},unique = true)})
public class Cliente {
    @PrimaryKey (autoGenerate = true)
    public int cid;
    public String nombre;
    public String fecha_nacimiento;
    public String direccion;
    public String sexo;
    public String telefono;
    public String correo;
    public String cedula;
    public String Observaciones;

    public int getId() {
        return cid;
    }
    public void setId(int cid) {
        this.cid = cid;
    }

    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getFecha_nacimiento() {
        return fecha_nacimiento;
    }
    public void setFecha_nacimiento(String fecha_nacimiento) { this.fecha_nacimiento = fecha_nacimiento; }

    public String getDireccion() {
        return direccion;
    }
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getSexo() {
        return sexo;
    }
    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getTelefono() {
        return telefono;
    }
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCorreo() {
        return correo;
    }
    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getCedula() {
        return cedula;
    }
    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getObservaciones() {
        return Observaciones;
    }
    public void setObservaciones(String observaciones) {
        Observaciones = observaciones;
    }


    public Cliente(int id,
                   String nombre,
                   String fecha_nacimiento,
                   String direccion,
                   String telefono,
                   String correo,
                   String cedula,
                   String sexo,
                   String observaciones) {
    }

    public Cliente() {
        this.cid = cid;
        this.nombre = nombre;
        this.fecha_nacimiento = fecha_nacimiento;
        this.direccion = direccion;
        this.sexo = sexo;
        this.telefono = telefono;
        this.correo = correo;
        this.cedula = cedula;
        this.Observaciones = Observaciones;
    }

}
