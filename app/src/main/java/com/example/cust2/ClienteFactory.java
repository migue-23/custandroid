package com.example.cust2;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.cust2.models.ClienteViewModel;

public class ClienteFactory extends ViewModelProvider.NewInstanceFactory {

    @NonNull
    private final Application application;

    public ClienteFactory (@NonNull Application application){
        this.application = application;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if(modelClass == ClienteViewModel.class){
            return (T) new ClienteViewModel(application);
        }
        return null;
    }
}
