package com.example.cust2.repositories;

import android.app.Application;

import androidx.lifecycle.LiveData;

import com.example.cust2.daos.ServicioClienteDao;
import com.example.cust2.database.AppDatabase;
import com.example.cust2.entities.ServicioCliente;

import java.util.List;

public class ServicioClienteRepository {
    private ServicioClienteDao servicioClienteDao;

    private LiveData<List<ServicioCliente>> serviciosCliente;
    private LiveData<List<ServicioCliente>> byUser;

    public ServicioClienteRepository(Application application) {
        AppDatabase db = AppDatabase.getInstance(application);
        servicioClienteDao = db.servicioClienteDao();
        serviciosCliente = servicioClienteDao.getAll();
    }

    public LiveData<List<ServicioCliente>> getServicioCliente() { return serviciosCliente; }

    public LiveData<List<ServicioCliente>> getServicioClienteByNombreUsuario(String nombreUsuario) {
        AppDatabase.databaseWriteExecutor.execute(() ->{
            byUser = servicioClienteDao.getServicioClienteByNombreUsuario(nombreUsuario);
        });
        return byUser;
    }

    public void insert(ServicioCliente servicioCliente){
        AppDatabase.databaseWriteExecutor.execute(() -> {
            servicioClienteDao.insert(servicioCliente);
        });
    }

    public void update(ServicioCliente servicioCliente){
        AppDatabase.databaseWriteExecutor.execute(() -> {
            servicioClienteDao.update(servicioCliente);
        });
    }

    public void delete(ServicioCliente servicioCliente){
        AppDatabase.databaseWriteExecutor.execute(() -> {
            servicioClienteDao.delete(servicioCliente);
        });
    }

}
