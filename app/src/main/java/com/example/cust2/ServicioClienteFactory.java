package com.example.cust2;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.cust2.models.ServicioClienteViewModel;

public class ServicioClienteFactory implements ViewModelProvider.Factory {

    @NonNull
    private final Application application;

    public ServicioClienteFactory(@NonNull Application application) {
        this.application = application;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass == ServicioClienteViewModel.class) {
            return (T) new ServicioClienteViewModel(application);
        }
        return null;
    }
}
