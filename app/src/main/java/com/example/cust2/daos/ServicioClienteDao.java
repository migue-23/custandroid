package com.example.cust2.daos;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.cust2.entities.ServicioCliente;

import java.util.List;

@Dao
public interface ServicioClienteDao {

    @Query("SELECT * FROM serviciocliente")
    LiveData<List<ServicioCliente>> getAll();

    @Query("SELECT * FROM serviciocliente where nombreUsuario like :nombreUsuario")
    LiveData<List<ServicioCliente>> getServicioClienteByNombreUsuario(String nombreUsuario);

    @Insert
    void insert(ServicioCliente servicioCliente);

    @Update
    void update(ServicioCliente servicioCliente);

    @Delete
    void delete(ServicioCliente servicioCliente);
}
