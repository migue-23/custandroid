package com.example.cust2.entities;

import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity (indices = {@Index(value = {"nombre"},unique = true)})
public class Servicio {

    @PrimaryKey (autoGenerate = true)
    public int sid;
    public String nombre;
    public String descripcion;


    public int getId() {
        return sid;
    }
    public void setId(int id) {
        this.sid = sid;
    }

    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Servicio(int id,
                    String nombre,
                    String descripcion) {
    }

    public Servicio() {
        this.sid = sid;
        this.nombre = nombre;
        this.descripcion = descripcion;
    }
}
