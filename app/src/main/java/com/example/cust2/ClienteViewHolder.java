package com.example.cust2;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

public class ClienteViewHolder  extends RecyclerView.ViewHolder {
    private final TextView nombreItemView;

    private ClienteViewHolder(View itemView){
        super(itemView);
        nombreItemView = itemView.findViewById(R.id.textViewNombreCliente);
    }

    public void bind(String nombre){
        nombreItemView.setText(nombre);
    }

    static ClienteViewHolder create(ViewGroup parent){
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cliente_item, parent, false);
        return new ClienteViewHolder(view);
    }
}