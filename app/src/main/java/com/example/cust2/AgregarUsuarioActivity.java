package com.example.cust2;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

public class AgregarUsuarioActivity extends AppCompatActivity {
    public static final String EXTRA_MSG_ID = "com.example.cust2.MSG_GUARDAR_USU ID";
    public static final String EXTRA_MSG_NOMBRE_USU = "com.example.cust2.MSG_GUARDAR_USU NOMBRE_CLI";
    public static final String EXTRA_MSG_FECHA_NACIMIENTO = "com.example.cust2.MSG_GUARDAR_USU FECHA_NACIMIENTO";
    public static final String EXTRA_MSG_DIRECCION = "com.example.cust2.MSG_GUARDAR_USU DIRECCION";
    public static final String EXTRA_MSG_TELEFONO = "com.example.cust2.MSG_GUARDAR_USU TELEFONO";
    public static final String EXTRA_MSG_CORREO = "com.example.cust2.MSG_GUARDAR_USU CORREO";
    public static final String EXTRA_MSG_CEDULA = "com.example.cust2.MSG_GUARDAR_USU CEDULA";
    public static final String EXTRA_MSG_SEXO = "com.example.cust2.MSG_GUARDAR_USU SEXO";
    public static final String EXTRA_MSG_OBSERVACIONES = "com.example.cust2.MSG_GUARDAR_USU OBSERVACIONES";
    public static final String EXTRA_MSG_PASSWORD = "com.example.cust2.MSG_GUARDAR_USU PASSWORD";

    private EditText editTextNombre;
    private EditText editTextFecha_Nacimiento;
    private EditText editTextDireccion;
    private EditText editTextTelefono;
    private EditText editTextCorreo;
    private EditText editTextCedula;
    private EditText editTextSexo;
    private EditText editTextObservaciones;
    private EditText editTextPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_usuario);

        editTextNombre = findViewById(R.id.textViewIngresarNombreUsuario);
        editTextFecha_Nacimiento = findViewById(R.id.textViewIngresarFecha_nacimientoUsuario);
        editTextDireccion = findViewById(R.id.textViewIngresarDireccionUsuario);
        editTextTelefono = findViewById(R.id.textViewIngresarTelefonoUsuario);
        editTextCorreo = findViewById(R.id.textViewIngresarCorreoUsuario);
        editTextCedula = findViewById(R.id.textViewIngresarCedulaUsuario);
        editTextSexo = findViewById(R.id.textViewIngresarSexoUsuario);
        editTextObservaciones = findViewById(R.id.textViewIngresarObservacionesUsuario);
        editTextPassword = findViewById(R.id.textViewIngresarPassword);

        Intent intent = getIntent();

        if(intent.hasExtra(EXTRA_MSG_ID)){

            editTextNombre.setText(intent.getStringExtra(EXTRA_MSG_NOMBRE_USU));
            editTextFecha_Nacimiento.setText(intent.getStringExtra(EXTRA_MSG_FECHA_NACIMIENTO));
            editTextDireccion.setText(intent.getStringExtra(EXTRA_MSG_DIRECCION));
            editTextTelefono.setText(intent.getStringExtra(EXTRA_MSG_TELEFONO));
            editTextCorreo.setText(intent.getStringExtra(EXTRA_MSG_CORREO));
            editTextCedula.setText(intent.getStringExtra(EXTRA_MSG_CEDULA));
            editTextSexo.setText(intent.getStringExtra(EXTRA_MSG_SEXO));
            editTextObservaciones.setText(intent.getStringExtra(EXTRA_MSG_OBSERVACIONES));
            editTextPassword.setText(intent.getStringExtra(EXTRA_MSG_PASSWORD));

        }

        final Button btnAgregar = findViewById(R.id.btnGuardar);
        btnAgregar.setOnClickListener(view -> {
            Intent respuesta = new Intent();

            if(TextUtils.isEmpty(editTextNombre.getText())){
                setResult(RESULT_CANCELED, respuesta);
            } else {

                String nombre = editTextNombre.getText().toString();
                String fecha_nacimiento = editTextFecha_Nacimiento.getText().toString();
                String direccion = editTextDireccion.getText().toString();
                String telefono = editTextTelefono.getText().toString();
                String correo = editTextCorreo.getText().toString();
                String cedula = editTextCedula.getText().toString();
                String sexo = editTextSexo.getText().toString();
                String observaciones = editTextObservaciones.getText().toString();
                String password = editTextPassword.getText().toString();

                respuesta.putExtra(EXTRA_MSG_NOMBRE_USU, nombre);
                respuesta.putExtra(EXTRA_MSG_FECHA_NACIMIENTO, fecha_nacimiento);
                respuesta.putExtra(EXTRA_MSG_DIRECCION, direccion);
                respuesta.putExtra(EXTRA_MSG_TELEFONO, telefono);
                respuesta.putExtra(EXTRA_MSG_CORREO, correo);
                respuesta.putExtra(EXTRA_MSG_CEDULA, cedula);
                respuesta.putExtra(EXTRA_MSG_SEXO, sexo);
                respuesta.putExtra(EXTRA_MSG_OBSERVACIONES, observaciones);
                respuesta.putExtra(EXTRA_MSG_PASSWORD, password);

                int id = getIntent().getIntExtra(EXTRA_MSG_ID, -1);
                if(id != -1){
                    respuesta.putExtra(EXTRA_MSG_ID, id);
                }
                setResult(RESULT_OK, respuesta);
            }

            finish();
        });
    }
}
