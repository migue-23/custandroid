package com.example.cust2;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.cust2.entities.Servicio;
import com.example.cust2.models.ServicioViewModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class ListaServicioActivity extends AppCompatActivity {

    private ServicioViewModel servicioViewModel;
    public static final int NEW_SERVICIO_REQ_CODE = 1;
    public static final int UPDATE_SERVICIO_REQ_CODE = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_servicio);

        RecyclerView recyclerView = findViewById(R.id.recyclerViewServicios);
        final ServicioListAdapter adapter = new ServicioListAdapter(new ServicioListAdapter.ServicioDiff());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        servicioViewModel = new ViewModelProvider(this, new ServicioFactory(getApplication())).get(ServicioViewModel.class);

        servicioViewModel.getServicio().observe(this, servicios -> {
            adapter.submitList(servicios);
        });

        FloatingActionButton fab = findViewById(R.id.btnAgregarServicio);
        fab.setOnClickListener(view -> {
            Intent intent = new Intent(ListaServicioActivity.this, AgregarServicioActivity.class);

            startActivityForResult(intent, NEW_SERVICIO_REQ_CODE);
        });

        adapter.setOnItemClickListener(new ServicioListAdapter.OnItemClickListener() {
            @Override
            public void onItemDelete(Servicio servicio) {
                servicioViewModel.delete(servicio);
                Toast.makeText(getApplicationContext(), R.string.borrado, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onItemClick(Servicio servicio) {
                Intent intent = new Intent(ListaServicioActivity.this, AgregarServicioActivity.class);
                intent.putExtra(AgregarServicioActivity.EXTRA_MSG_NOMBRE_SERV, servicio.getNombre());
                intent.putExtra(AgregarServicioActivity.EXTRA_MSG_DESCRIPCION_SERV, servicio.getDescripcion());
                intent.putExtra(AgregarServicioActivity.EXTRA_MSG_ID_SERV, servicio.getId());
                startActivityForResult(intent, UPDATE_SERVICIO_REQ_CODE);
            }
        });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode==NEW_SERVICIO_REQ_CODE && resultCode == RESULT_OK){
            Servicio servicio = new Servicio();
            servicio.setNombre(data.getStringExtra(AgregarServicioActivity.EXTRA_MSG_NOMBRE_SERV));
            servicio.setDescripcion(data.getStringExtra(AgregarServicioActivity.EXTRA_MSG_DESCRIPCION_SERV));
            servicioViewModel.insert(servicio);
        } else if (requestCode == UPDATE_SERVICIO_REQ_CODE && resultCode == RESULT_OK){
            int id = data.getIntExtra(AgregarServicioActivity.EXTRA_MSG_ID_SERV, -1);
            if (id == -1){
                Toast.makeText(getApplicationContext(), R.string.no_actualizado, Toast.LENGTH_LONG).show();
            }
            String nombre = data.getStringExtra(AgregarServicioActivity.EXTRA_MSG_NOMBRE_SERV);
            String descripcion = data.getStringExtra(AgregarServicioActivity.EXTRA_MSG_DESCRIPCION_SERV);

            Servicio servicio = new Servicio(id, nombre, descripcion);
            servicioViewModel.update(servicio);

        } else {
            Toast.makeText(getApplicationContext(), R.string.no_guardado, Toast.LENGTH_LONG).show();
        }
    }

}