package com.example.cust2;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

public class MainActivity2 extends AppCompatActivity implements View.OnClickListener {

    public CardView card_view_Colaboradores;
    public CardView card_view_Clientes;
    public CardView card_view_Servicios;
    public CardView card_view_ServiciosRealizados;
    public CardView card_view_NuevoServicio;
    public ImageButton icoLogout;
    public String extra;

    TextView loggin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        loggin = findViewById(R.id.loggin);

        Intent intent = getIntent();

        if (intent.hasExtra("UserLogged")) {
            extra = intent.getStringExtra("UserLogged");
            String loggedAs = "Usuario: " + extra;
            loggin.setText(loggedAs);
        }

        card_view_Colaboradores = findViewById(R.id.card_view_Colaboradores);
        card_view_Colaboradores.setOnClickListener(this);

        card_view_Clientes = findViewById(R.id.card_view_Clientes);
        card_view_Clientes.setOnClickListener(this);

        card_view_Servicios = findViewById(R.id.card_view_Servicios);
        card_view_Servicios.setOnClickListener(this);

        card_view_ServiciosRealizados = findViewById(R.id.card_view_ServiciosRealizados);
        card_view_ServiciosRealizados.setOnClickListener(this);

        icoLogout = findViewById(R.id.icoLogout);
        icoLogout.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent i;

        switch (v.getId()) {
            case R.id.card_view_Colaboradores :
                i = new Intent(this, ListaUsuarioActivity.class);
                startActivity(i);
                break;

            case R.id.card_view_Clientes :
                i = new Intent(this, ListaClienteActivity.class);
                startActivity(i);
                break;

            case R.id.card_view_Servicios:
                i = new Intent(this, ListaServicioActivity.class);
                startActivity(i);
                break;

            case R.id.card_view_ServiciosRealizados:
                i = new Intent(this, ListaServicioRealizadoActivity.class);
                i.putExtra("UserLogged", extra);
                startActivity(i);
                break;

            case R.id.icoLogout:
                i = new Intent(this, MainActivity.class);
                startActivity(i);
                finish();
        }
    }
}
