package com.example.cust2.repositories;

// import androidx.annotation.WorkerThread;
import android.app.Application;

import androidx.lifecycle.LiveData;

import com.example.cust2.daos.ClienteDao;
import com.example.cust2.database.AppDatabase;
import com.example.cust2.entities.Cliente;


import java.util.List;

public class ClienteRepository {
    private ClienteDao clienteDao;

    private LiveData<List<Cliente>> clientes;
    private LiveData<Cliente> cliente;

    public ClienteRepository(Application application){
        AppDatabase db = AppDatabase.getInstance(application);
        clienteDao = db.clienteDao();
        clientes = clienteDao.getAll();
    }

    public LiveData<List<Cliente>> getCliente(){
        return clientes;
    }

    public void insert(Cliente cliente){
        AppDatabase.databaseWriteExecutor.execute(() -> {
            clienteDao.insert(cliente);
        });
    }

    public void update(Cliente cliente){
        AppDatabase.databaseWriteExecutor.execute(() -> {
            clienteDao.update(cliente);
        });
    }

    public void delete(Cliente cliente){
        AppDatabase.databaseWriteExecutor.execute(() -> {
            clienteDao.delete(cliente);
        });
    }

}

