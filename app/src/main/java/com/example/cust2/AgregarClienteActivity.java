package com.example.cust2;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

public class  AgregarClienteActivity extends AppCompatActivity {
    public static final String EXTRA_MSG_ID = "com.example.cust2.MSG_GUARDAR_CLI ID";
    public static final String EXTRA_MSG_NOMBRE_CLI = "com.example.cust2.MSG_GUARDAR_CLI NOMBRE_CLI";
    public static final String EXTRA_MSG_FECHA_NACIMIENTO = "com.example.cust2.MSG_GUARDAR_CLI FECHA_NACIMIENTO";
    public static final String EXTRA_MSG_DIRECCION = "com.example.cust2.MSG_GUARDAR_CLI DIRECCION";
    public static final String EXTRA_MSG_TELEFONO = "com.example.cust2.MSG_GUARDAR_CLI TELEFONO";
    public static final String EXTRA_MSG_CORREO = "com.example.cust2.MSG_GUARDAR_CLI CORREO";
    public static final String EXTRA_MSG_CEDULA = "com.example.cust2.MSG_GUARDAR_CLI CEDULA";
    public static final String EXTRA_MSG_SEXO = "com.example.cust2.MSG_GUARDAR_CLI SEXO";
    public static final String EXTRA_MSG_OBSERVACIONES = "com.example.cust2.MSG_GUARDAR_CLI OBSERVACIONES";

    private EditText editTextNombre;
    private EditText editTextFecha_Nacimiento;
    private EditText editTextDireccion;
    private EditText editTextTelefono;
    private EditText editTextCorreo;
    private EditText editTextCedula;
    private EditText editTextSexo;
    private EditText editTextObservaciones;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_cliente);

        editTextNombre = findViewById(R.id.textViewIngresarNombreCliente);
        editTextFecha_Nacimiento = findViewById(R.id.textViewIngresarFecha_nacimientoCliente);
        editTextDireccion = findViewById(R.id.textViewIngresarDireccionCliente);
        editTextTelefono = findViewById(R.id.textViewIngresarTelefonoCliente);
        editTextCorreo = findViewById(R.id.textViewIngresarCorreoCliente);
        editTextCedula = findViewById(R.id.textViewIngresarCedulaCliente);
        editTextSexo = findViewById(R.id.textViewIngresarSexoCliente);
        editTextObservaciones = findViewById(R.id.textViewIngresarObservacionesCliente);



        final Button btnAgregar = findViewById(R.id.btnGuardarCliente);
        btnAgregar.setOnClickListener(view -> {
            Intent respuesta = new Intent();
            if(TextUtils.isEmpty(editTextNombre.getText())){
                setResult(RESULT_CANCELED, respuesta);
            } else {
                String nombre = editTextNombre.getText().toString();
                String fecha_nacimiento = editTextFecha_Nacimiento.getText().toString();
                String direccion = editTextDireccion.getText().toString();
                String telefono = editTextTelefono.getText().toString();
                String correo = editTextCorreo.getText().toString();
                String cedula = editTextCedula.getText().toString();
                String sexo = editTextSexo.getText().toString();
                String observaciones = editTextObservaciones.getText().toString();

                respuesta.putExtra(EXTRA_MSG_NOMBRE_CLI, nombre);
                respuesta.putExtra(EXTRA_MSG_FECHA_NACIMIENTO, fecha_nacimiento);
                respuesta.putExtra(EXTRA_MSG_DIRECCION, direccion);
                respuesta.putExtra(EXTRA_MSG_TELEFONO, telefono);
                respuesta.putExtra(EXTRA_MSG_CORREO, correo);
                respuesta.putExtra(EXTRA_MSG_CEDULA, cedula);
                respuesta.putExtra(EXTRA_MSG_SEXO, sexo);
                respuesta.putExtra(EXTRA_MSG_OBSERVACIONES, observaciones);

                int id = getIntent().getIntExtra(EXTRA_MSG_ID, -1);
                if(id != -1){
                    respuesta.putExtra(EXTRA_MSG_ID, id);
                }
                setResult(RESULT_OK, respuesta);
            }
            finish();
        });

        Intent intent = getIntent();

        if(intent.hasExtra(EXTRA_MSG_ID)){
            editTextNombre.setText(intent.getStringExtra(EXTRA_MSG_NOMBRE_CLI));
            editTextFecha_Nacimiento.setText(intent.getStringExtra(EXTRA_MSG_FECHA_NACIMIENTO));
            editTextDireccion.setText(intent.getStringExtra(EXTRA_MSG_DIRECCION));
            editTextTelefono.setText(intent.getStringExtra(EXTRA_MSG_TELEFONO));
            editTextCorreo.setText(intent.getStringExtra(EXTRA_MSG_CORREO));
            editTextCedula.setText(intent.getStringExtra(EXTRA_MSG_CEDULA));
            editTextSexo.setText(intent.getStringExtra(EXTRA_MSG_SEXO));
            editTextObservaciones.setText(intent.getStringExtra(EXTRA_MSG_OBSERVACIONES));
        }
    }
}

