package com.example.cust2;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.cust2.entities.Cliente;
import com.example.cust2.models.ClienteViewModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class ListaClienteActivity extends AppCompatActivity {

    private ClienteViewModel clienteViewModel;
    public static final int NEW_CLIENTE_REQ_CODE = 1;
    public static final int UPDATE_CLIENTE_REQ_CODE = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_cliente);

        RecyclerView recyclerView = findViewById(R.id.recyclerViewClientes);
        final ClienteListAdapter adapter = new ClienteListAdapter(new ClienteListAdapter.ClienteDiff());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        clienteViewModel = new ViewModelProvider(this, new ClienteFactory(getApplication())).get(ClienteViewModel.class);

        clienteViewModel.getCliente().observe(this, clientes ->  {
            adapter.submitList(clientes);
        });

        FloatingActionButton fab = findViewById(R.id.btnAgregarCliente);
        fab.setOnClickListener(view -> {
            Intent intent = new Intent(ListaClienteActivity.this, AgregarClienteActivity.class);

            startActivityForResult(intent, NEW_CLIENTE_REQ_CODE);
        });

        adapter.setOnItemClickListener(new ClienteListAdapter.OnItemClickListener() {
            @Override
            public void onItemDelete(Cliente cliente) {
                clienteViewModel.delete(cliente);
                Toast.makeText(getApplicationContext(), R.string.borrado, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onItemClick(Cliente cliente) {
                Intent intent = new Intent(ListaClienteActivity.this, AgregarClienteActivity.class);
                intent.putExtra(AgregarClienteActivity.EXTRA_MSG_NOMBRE_CLI, cliente.getNombre());
                intent.putExtra(AgregarClienteActivity.EXTRA_MSG_FECHA_NACIMIENTO, cliente.getFecha_nacimiento());
                intent.putExtra(AgregarClienteActivity.EXTRA_MSG_ID, cliente.getId());
                intent.putExtra(AgregarClienteActivity.EXTRA_MSG_DIRECCION, cliente.getDireccion());
                intent.putExtra(AgregarClienteActivity.EXTRA_MSG_TELEFONO, cliente.getTelefono());
                intent.putExtra(AgregarClienteActivity.EXTRA_MSG_CORREO, cliente.getCorreo());
                intent.putExtra(AgregarClienteActivity.EXTRA_MSG_CEDULA, cliente.getCedula());
                intent.putExtra(AgregarClienteActivity.EXTRA_MSG_SEXO, cliente.getSexo());
                intent.putExtra(AgregarClienteActivity.EXTRA_MSG_OBSERVACIONES, cliente.getObservaciones());
                startActivityForResult(intent, UPDATE_CLIENTE_REQ_CODE);
            }
        });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode==NEW_CLIENTE_REQ_CODE && resultCode == RESULT_OK){
            Cliente cliente = new Cliente();

            cliente.setNombre(data.getStringExtra(AgregarClienteActivity.EXTRA_MSG_NOMBRE_CLI));
            cliente.setFecha_nacimiento(data.getStringExtra(AgregarClienteActivity.EXTRA_MSG_FECHA_NACIMIENTO));
            cliente.setDireccion(data.getStringExtra(AgregarClienteActivity.EXTRA_MSG_DIRECCION));
            cliente.setTelefono(data.getStringExtra(AgregarClienteActivity.EXTRA_MSG_TELEFONO));
            cliente.setCorreo(data.getStringExtra(AgregarClienteActivity.EXTRA_MSG_CORREO));
            cliente.setCedula(data.getStringExtra(AgregarClienteActivity.EXTRA_MSG_CEDULA));
            cliente.setSexo(data.getStringExtra(AgregarClienteActivity.EXTRA_MSG_SEXO));
            cliente.setObservaciones(data.getStringExtra(AgregarClienteActivity.EXTRA_MSG_OBSERVACIONES));
            clienteViewModel.insert(cliente);
        } else if (requestCode == UPDATE_CLIENTE_REQ_CODE && resultCode == RESULT_OK){
            int id = data.getIntExtra(AgregarClienteActivity.EXTRA_MSG_ID, -1);
            if (id == -1){
                Toast.makeText(getApplicationContext(), R.string.no_actualizado, Toast.LENGTH_LONG).show();
            }
            String nombre = data.getStringExtra(AgregarClienteActivity.EXTRA_MSG_NOMBRE_CLI);
            String fecha_nacimiento = data.getStringExtra(AgregarClienteActivity.EXTRA_MSG_FECHA_NACIMIENTO);
            String direccion = data.getStringExtra(AgregarClienteActivity.EXTRA_MSG_DIRECCION);
            String telefono = data.getStringExtra(AgregarClienteActivity.EXTRA_MSG_TELEFONO);
            String correo = data.getStringExtra(AgregarClienteActivity.EXTRA_MSG_CORREO);
            String cedula = data.getStringExtra(AgregarClienteActivity.EXTRA_MSG_CEDULA);
            String sexo = data.getStringExtra(AgregarClienteActivity.EXTRA_MSG_SEXO);
            String observaciones = data.getStringExtra(AgregarClienteActivity.EXTRA_MSG_OBSERVACIONES);

            Cliente cliente = new Cliente(id, nombre, fecha_nacimiento, direccion, telefono, correo, cedula, sexo, observaciones);
            clienteViewModel.update(cliente);

        } else {
            Toast.makeText(getApplicationContext(), R.string.no_guardado, Toast.LENGTH_LONG).show();
        }
    }

}



