package com.example.cust2.models;


import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.cust2.entities.Cliente;

import com.example.cust2.repositories.ClienteRepository;

import java.util.List;

public class ClienteViewModel extends AndroidViewModel {

    private ClienteRepository clienteRepository;
    private final LiveData<List<Cliente>> cliente;

    public ClienteViewModel(Application application){
        super(application);
        clienteRepository = new ClienteRepository(application);
        cliente = clienteRepository.getCliente();
    }

    public LiveData<List<Cliente>> getCliente(){
        return cliente;
    }

    public void insert(Cliente cliente) { clienteRepository.insert(cliente); }

    public void update(Cliente cliente) { clienteRepository.update(cliente); }

    public void delete(Cliente cliente) { clienteRepository.delete(cliente); }

}
