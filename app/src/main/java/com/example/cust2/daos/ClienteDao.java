package com.example.cust2.daos;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.cust2.entities.Cliente;

import java.util.List;

@Dao

public interface ClienteDao {

    @Query("SELECT * FROM cliente")
    LiveData<List<Cliente>> getAll();

    @Insert
    void insert(Cliente cliente);

    @Update
    void update(Cliente cliente);

    @Delete
    void delete(Cliente cliente);

    @Query("SELECT * FROM cliente where cid = :cid")
    Cliente findById(int cid);

}


