package com.example.cust2;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

public class ServicioClienteViewHolder extends RecyclerView.ViewHolder {
    private final TextView nombreServicioItemView;
    private final TextView nombreClienteItemView;

    private ServicioClienteViewHolder(View itemView) {
        super(itemView);
        nombreServicioItemView = itemView.findViewById(R.id.tvNombreServicioList);
        nombreClienteItemView = itemView.findViewById(R.id.tvNombreClienteList);
    }

    public void bind(String servicio, String cliente) {
        nombreServicioItemView.setText(servicio);
        nombreClienteItemView.setText(cliente);
    }

    static ServicioClienteViewHolder create(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.servicio_cliente_item, parent, false);
        return new ServicioClienteViewHolder(view);
    }
}
