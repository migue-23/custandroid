package com.example.cust2;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

public class AgregarServicioActivity extends AppCompatActivity {
    public static final String EXTRA_MSG_ID_SERV = "com.example.cust2.MSG_GUARDAR_SERV ID";
    public static final String EXTRA_MSG_NOMBRE_SERV = "com.example.cust2.MSG_GUARDAR_SERV NOMBRE SERV";
    public static final String EXTRA_MSG_DESCRIPCION_SERV = "com.example.cust2.MSG_GUARDAR_SERV DESCRIPCION SERV";

    private EditText editTextNombre;
    private EditText editTextDescripcion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_servicio);

        editTextNombre = findViewById(R.id.textViewIngresarNombre);
        editTextDescripcion = findViewById(R.id.textViewIngresarDescripcion);

        final Button btnAgregar = findViewById(R.id.btnGuardar);
        btnAgregar.setOnClickListener(view -> {
            Intent respuesta = new Intent();
            if(TextUtils.isEmpty(editTextNombre.getText())){
                setResult(RESULT_CANCELED, respuesta);
            } else {
                String servicio = editTextNombre.getText().toString();
                String descripcion = editTextDescripcion.getText().toString();
                respuesta.putExtra(EXTRA_MSG_NOMBRE_SERV, servicio);
                respuesta.putExtra(EXTRA_MSG_DESCRIPCION_SERV, descripcion);

                int id = getIntent().getIntExtra(EXTRA_MSG_ID_SERV, -1);
                if(id != -1){
                    respuesta.putExtra(EXTRA_MSG_ID_SERV, id);
                }
                setResult(RESULT_OK, respuesta);
            }
            finish();
        });

        Intent intent = getIntent();

        if(intent.hasExtra(EXTRA_MSG_ID_SERV)){
            editTextNombre.setText(intent.getStringExtra(EXTRA_MSG_NOMBRE_SERV));
            editTextDescripcion.setText(intent.getStringExtra(EXTRA_MSG_DESCRIPCION_SERV));
        }
    }
}