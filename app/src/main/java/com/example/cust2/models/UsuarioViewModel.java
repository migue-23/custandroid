package com.example.cust2.models;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.cust2.entities.Usuario;
import com.example.cust2.repositories.UsuarioRepository;

import java.util.List;

public class UsuarioViewModel extends AndroidViewModel {

    private UsuarioRepository usuarioRepository;
    private final LiveData<List<Usuario>> usuario;

    public UsuarioViewModel(Application application){
        super(application);
        usuarioRepository = new UsuarioRepository(application);
        usuario = usuarioRepository.getUsuario();
    }

    public LiveData<List<Usuario>> getUsuario(){
        return usuario;
    }

    public void insert(Usuario usuario) { usuarioRepository.insert(usuario); }

    public void update(Usuario usuario) {
            usuarioRepository.update(usuario);
    }

    public void delete(Usuario usuario) {
        usuarioRepository.delete(usuario);
    }

    public Usuario login(String user, String password) { return usuarioRepository.login(user, password); }
}
