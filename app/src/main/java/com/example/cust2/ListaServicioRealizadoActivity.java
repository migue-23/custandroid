package com.example.cust2;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.cust2.entities.ServicioCliente;
import com.example.cust2.models.ServicioClienteViewModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import static com.example.cust2.AgregarServicioClienteActivity.EXTRA_MSG_NOMBRE_USUARIO;

public class ListaServicioRealizadoActivity extends AppCompatActivity {

    private ServicioClienteViewModel servicioClienteViewModel;
    public static final int NEW_SERVICIO_CLIENTE_REQ_CODE = 1;
    public static final int UPDATE_SERVICIO_CLIENTE_REQ_CODE = 2;
    public String extra;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_servicio_realizado);

        Intent mainIntent = getIntent();

        if (mainIntent.hasExtra("UserLogged")) {
            extra = mainIntent.getStringExtra("UserLogged");
        }

        RecyclerView recyclerView = findViewById(R.id.recyclerViewServiciosClientes);
        final ServicioClienteListAdapter adapter = new ServicioClienteListAdapter(new ServicioClienteListAdapter.ServicioClienteDiff());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        servicioClienteViewModel = new ViewModelProvider(this, new ServicioClienteFactory(getApplication())).get(ServicioClienteViewModel.class);

        /*servicioClienteViewModel.getServicioClienteByNombreUsuario(extra).observe(this, adapter::submitList);*/

        servicioClienteViewModel.getServicioCliente().observe(this, adapter::submitList);

        FloatingActionButton fab = findViewById(R.id.btnAgregarServicioCliente);
        fab.setOnClickListener(view -> {
            Intent intent = new Intent(ListaServicioRealizadoActivity.this, AgregarServicioClienteActivity.class);
            intent.putExtra(EXTRA_MSG_NOMBRE_USUARIO, extra);
            startActivityForResult(intent, NEW_SERVICIO_CLIENTE_REQ_CODE);
        });

        adapter.setOnItemClickListener(new ServicioClienteListAdapter.OnItemClickListener() {
            @Override
            public void onItemDelete(ServicioCliente servicioCliente) {
                servicioClienteViewModel.delete(servicioCliente);
                Toast.makeText(getApplicationContext(), R.string.borrado, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onItemClick(ServicioCliente servicioCliente) {

                Intent intent = new Intent(ListaServicioRealizadoActivity.this, AgregarServicioClienteActivity.class);
                intent.putExtra(AgregarServicioClienteActivity.EXTRA_MSG_ID_SERV_CLIENT, servicioCliente.getScid());
                intent.putExtra(AgregarServicioClienteActivity.EXTRA_MSG_NOMBRE_CLIENT, servicioCliente.getNombreCliente());
                intent.putExtra(AgregarServicioClienteActivity.EXTRA_MSG_NOMBRE_SERV, servicioCliente.getNombreServicio());
                intent.putExtra(AgregarServicioClienteActivity.EXTRA_MSG_NOMBRE_USUARIO, servicioCliente.getNombreUsuario());
                startActivityForResult(intent, UPDATE_SERVICIO_CLIENTE_REQ_CODE);
            }
        });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == NEW_SERVICIO_CLIENTE_REQ_CODE && resultCode == RESULT_OK) {
            ServicioCliente servicioCliente = new ServicioCliente();
            servicioCliente.setNombreCliente(data.getStringExtra(AgregarServicioClienteActivity.EXTRA_MSG_NOMBRE_CLIENT));
            servicioCliente.setNombreServicio(data.getStringExtra(AgregarServicioClienteActivity.EXTRA_MSG_NOMBRE_SERV));
            servicioCliente.setNombreUsuario(data.getStringExtra(AgregarServicioClienteActivity.EXTRA_MSG_NOMBRE_USUARIO));
            servicioClienteViewModel.insert(servicioCliente);
        } else if (requestCode == UPDATE_SERVICIO_CLIENTE_REQ_CODE && resultCode == RESULT_OK) {
            int id = data.getIntExtra(AgregarServicioClienteActivity.EXTRA_MSG_ID_SERV_CLIENT, -1);
            if (id == -1) {
                Toast.makeText(getApplicationContext(), R.string.no_actualizado, Toast.LENGTH_LONG).show();
            }
            String nombreCliente = data.getStringExtra(AgregarServicioClienteActivity.EXTRA_MSG_NOMBRE_CLIENT);
            String nombreServicio = data.getStringExtra(AgregarServicioClienteActivity.EXTRA_MSG_NOMBRE_SERV);
            String nombreUsuario = data.getStringExtra(AgregarServicioClienteActivity.EXTRA_MSG_NOMBRE_USUARIO);

            ServicioCliente servicioCliente = new ServicioCliente(id, nombreServicio, nombreCliente, nombreUsuario);
            servicioClienteViewModel.update(servicioCliente);
        } else {
            Toast.makeText(getApplicationContext(), R.string.no_guardado, Toast.LENGTH_LONG).show();
        }
    }
}