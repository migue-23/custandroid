package com.example.cust2.models;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.cust2.entities.Servicio;
import com.example.cust2.repositories.ServicioRepository;

import java.util.List;

public class ServicioViewModel extends AndroidViewModel {

    private ServicioRepository servicioRepository;
    private final LiveData<List<Servicio>> servicio;

    public ServicioViewModel(Application application){
        super(application);
        servicioRepository = new ServicioRepository(application);
        servicio = servicioRepository.getServicio();
    }

    public LiveData<List<Servicio>> getServicio(){
        return servicio;
    }

    public void insert(Servicio servicio) { servicioRepository.insert(servicio); }

    public void update(Servicio servicio) {
        servicioRepository.update(servicio);
    }

    public void delete(Servicio servicio) {
        servicioRepository.delete(servicio);
    }

}
