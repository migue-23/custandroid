package com.example.cust2.models;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.cust2.entities.ServicioCliente;
import com.example.cust2.repositories.ServicioClienteRepository;

import java.util.List;

public class ServicioClienteViewModel extends AndroidViewModel {

    private ServicioClienteRepository servicioClienteRepository;
    private final LiveData<List<ServicioCliente>> servicioCliente;

    public ServicioClienteViewModel(Application application) {
        super(application);
        servicioClienteRepository = new ServicioClienteRepository(application);
        servicioCliente = servicioClienteRepository.getServicioCliente();
    }


    public LiveData<List<ServicioCliente>> getServicioCliente(){
        return servicioCliente;
    }

    public LiveData<List<ServicioCliente>> getServicioClienteByNombreUsuario(String nombreUsuario) {
        return servicioClienteRepository.getServicioClienteByNombreUsuario(nombreUsuario);
    }

    public void insert(ServicioCliente servicioCliente) { servicioClienteRepository.insert(servicioCliente); }

    public void update(ServicioCliente servicioCliente) { servicioClienteRepository.update(servicioCliente); }

    public void delete(ServicioCliente servicioCliente) { servicioClienteRepository.delete(servicioCliente); }
}
