package com.example.cust2;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;

import com.example.cust2.entities.Usuario;

public class UsuarioListAdapter extends ListAdapter<Usuario,UsuarioViewHolder> {

    private OnItemClickListener listener;

    public UsuarioListAdapter(@NonNull DiffUtil.ItemCallback<Usuario> diffCallbak){
        super(diffCallbak);
    }

    @NonNull
    @Override
    public UsuarioViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return UsuarioViewHolder.create(parent);
    }

    @Override
    public void onBindViewHolder(@NonNull UsuarioViewHolder holder, int position) {
        Usuario usuarioActual = getItem(position);
        holder.bind(usuarioActual.getNombre());

        ImageButton deleteButton = holder.itemView.findViewById(R.id.imageButtonDelete);
        deleteButton.setOnClickListener(View -> {
            if(listener!=null){
                listener.onItemDelete(usuarioActual);
            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listener!=null){
                    listener.onItemClick(usuarioActual);
                }
            }
        });
    }

    static class UsuarioDiff extends DiffUtil.ItemCallback<Usuario>{
        @Override
        public boolean areItemsTheSame(@NonNull Usuario oldItem, @NonNull Usuario newItem) {
            return oldItem.getUid() == newItem.getUid();
        }

        @Override
        public boolean areContentsTheSame(@NonNull Usuario oldItem, @NonNull Usuario newItem) {
            return oldItem.getNombre().equals(newItem.getNombre())
                    && oldItem.getFecha_nacimiento().equals(newItem.getFecha_nacimiento())
                    && oldItem.getDireccion().equals(newItem.getDireccion())
                    && oldItem.getTelefono().equals(newItem.getTelefono())
                    && oldItem.getCorreo().equals(newItem.getCorreo())
                    && oldItem.getCedula().equals(newItem.getCedula())
                    && oldItem.getSexo().equals(newItem.getSexo())
                    && oldItem.getObservaciones().equals(newItem.getObservaciones())
                    &&  oldItem.getPassword().equals(newItem.getPassword());
        }
    }

    public interface OnItemClickListener {
        void onItemDelete(Usuario usuario);
        void onItemClick(Usuario usuario);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }
}
