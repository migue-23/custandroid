package com.example.cust2;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.cust2.entities.Cliente;
import com.example.cust2.entities.Servicio;
import com.example.cust2.models.ClienteViewModel;
import com.example.cust2.models.ServicioViewModel;

public class AgregarServicioClienteActivity extends AppCompatActivity {
    public static final String EXTRA_MSG_ID_SERV_CLIENT = "com.example.cust2.MSG_GUARDAR_SERV_CLIENT ID";
    public static final String EXTRA_MSG_NOMBRE_SERV = "com.example.cust2.MSG_GUARDAR_SERV_CLIENT NOMBRE SERV";
    public static final String EXTRA_MSG_NOMBRE_CLIENT = "com.example.cust2.MSG_GUARDAR_SERV_CLIENT NOMBRE CLIENT";
    public static final String EXTRA_MSG_NOMBRE_USUARIO = "com.example.cust2.MSG_GUARDAR_SERV_CLIENT NOMBRE USUARIO";


    private TextView tv_NombreCliente, tv_NombreServicio, tv_NombreUsuario;
    RecyclerView recyclerViewClientes2, recyclerViewServicios2;
    private ClienteViewModel clienteViewModel;
    private ServicioViewModel servicioViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_servicio_cliente);

        tv_NombreCliente = findViewById(R.id.tv_NombreCliente);
        tv_NombreServicio = findViewById(R.id.tv_NombreServicio);
        tv_NombreUsuario = findViewById(R.id.tv_NombreUsuario);

        recyclerViewClientes2 = findViewById(R.id.recyclerViewClientes2);
        final ClienteListAdapter clientAdapter = new ClienteListAdapter(new ClienteListAdapter.ClienteDiff());
        recyclerViewClientes2.setAdapter(clientAdapter);
        recyclerViewClientes2.setLayoutManager(new LinearLayoutManager(this));

        clienteViewModel = new ViewModelProvider(this, new ClienteFactory(getApplication())).get(ClienteViewModel.class);

        clienteViewModel.getCliente().observe(this, clientes -> {
            clientAdapter.submitList(clientes);
        });

        clientAdapter.setOnItemClickListener(new ClienteListAdapter.OnItemClickListener() {
            @Override
            public void onItemDelete(Cliente cliente) {
                Toast.makeText(getApplicationContext(), "No puede Borrar un Cliente desde esta Vista", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onItemClick(Cliente cliente) {
                tv_NombreCliente.setText(cliente.getNombre());
            }
        });

        recyclerViewServicios2 = findViewById(R.id.recyclerViewServicios2);
        final ServicioListAdapter servicioAdapter = new ServicioListAdapter(new ServicioListAdapter.ServicioDiff());
        recyclerViewServicios2.setAdapter(servicioAdapter);
        recyclerViewServicios2.setLayoutManager(new LinearLayoutManager(this));

        servicioViewModel = new ViewModelProvider(this, new ServicioFactory(getApplication())).get(ServicioViewModel.class);

        servicioViewModel.getServicio().observe(this, servicios -> {
            servicioAdapter.submitList(servicios);
        });

        servicioAdapter.setOnItemClickListener(new ServicioListAdapter.OnItemClickListener() {
            @Override
            public void onItemDelete(Servicio servicio) {
                Toast.makeText(getApplicationContext(), "No puede Borrar un Servicio desde esta Vista", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onItemClick(Servicio servicio) {
                tv_NombreServicio.setText(servicio.getNombre());
            }
        });

        Intent intent = getIntent();

        if (intent.hasExtra(EXTRA_MSG_ID_SERV_CLIENT)) {
            tv_NombreServicio.setText(intent.getStringExtra(EXTRA_MSG_NOMBRE_SERV));
            tv_NombreCliente.setText(intent.getStringExtra(EXTRA_MSG_NOMBRE_CLIENT));
            tv_NombreUsuario.setText(intent.getStringExtra(EXTRA_MSG_NOMBRE_USUARIO));
        } else {
            tv_NombreUsuario.setText(intent.getStringExtra(EXTRA_MSG_NOMBRE_USUARIO));
        }

        final Button btnGuardarServicioCliente = findViewById(R.id.btnGuardarServicioCliente);
        btnGuardarServicioCliente.setOnClickListener(v -> {
            Intent respuesta = new Intent();
            if (TextUtils.isEmpty(tv_NombreCliente.getText()) || TextUtils.isEmpty(tv_NombreServicio.getText())) {
                setResult(RESULT_CANCELED, respuesta);
            } else {
                String nombreServicio = tv_NombreServicio.getText().toString();
                String nombreCliente = tv_NombreCliente.getText().toString();
                String nombreUsuario = tv_NombreUsuario.getText().toString();
                respuesta.putExtra(EXTRA_MSG_NOMBRE_CLIENT, nombreCliente);
                respuesta.putExtra(EXTRA_MSG_NOMBRE_SERV, nombreServicio);
                respuesta.putExtra(EXTRA_MSG_NOMBRE_USUARIO, nombreUsuario);

                int id = getIntent().getIntExtra(EXTRA_MSG_ID_SERV_CLIENT, -1);
                if (id != -1) {
                    respuesta.putExtra(EXTRA_MSG_ID_SERV_CLIENT, id);
                }
                setResult(RESULT_OK, respuesta);
            }
            finish();
        });
    }
}