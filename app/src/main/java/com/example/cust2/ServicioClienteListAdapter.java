package com.example.cust2;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;

import com.example.cust2.entities.ServicioCliente;

public class ServicioClienteListAdapter extends ListAdapter<ServicioCliente, ServicioClienteViewHolder> {

    private OnItemClickListener listener;

    public ServicioClienteListAdapter(@NonNull DiffUtil.ItemCallback<ServicioCliente> diffCallback) {
        super(diffCallback);
    }

    @NonNull
    @Override
    public ServicioClienteViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return ServicioClienteViewHolder.create(parent);
    }

    @Override
    public void onBindViewHolder(@NonNull ServicioClienteViewHolder holder, int position) {
        ServicioCliente servicioClienteActual = getItem(position);
        holder.bind(servicioClienteActual.getNombreServicio(), servicioClienteActual.getNombreCliente());

        ImageButton deleteButton = holder.itemView.findViewById(R.id.imageButtonDelete);
        deleteButton.setOnClickListener(view -> {
            if (listener != null) {
                listener.onItemDelete(servicioClienteActual);
            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onItemClick(servicioClienteActual);
                }
            }
        });
    }

    static class ServicioClienteDiff extends DiffUtil.ItemCallback<ServicioCliente> {
        @Override
        public boolean areItemsTheSame(@NonNull ServicioCliente oldItem, @NonNull ServicioCliente newItem){
            return oldItem.getScid() == newItem.getScid();
        }

        @Override
        public boolean areContentsTheSame(@NonNull ServicioCliente oldItem, @NonNull ServicioCliente newItem) {
            return oldItem.getNombreServicio().equals(newItem.getNombreServicio()) && oldItem.getNombreCliente().equals(newItem.getNombreCliente());
        }
    }

    public interface OnItemClickListener {
        void onItemDelete(ServicioCliente servicioCliente);
        void onItemClick(ServicioCliente servicioCliente);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }
}
