package com.example.cust2.daos;

//import androidx.lifecycle.LiveData;
import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.cust2.entities.Servicio;
import java.util.List;

@Dao
public interface ServicioDao {

    @Query("SELECT * FROM servicio")
    LiveData<List<Servicio>> getAll();

    @Insert
    void insert(Servicio servicio);

    @Update
    void update(Servicio servicio);

    @Delete
    void delete(Servicio servicio);

    @Query("SELECT * FROM servicio where nombre like :nombre")
    Servicio findByNombre(String nombre);

    @Query("SELECT * FROM servicio where sid = :sid")
    Servicio findById(int sid);

}