package com.example.cust2;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

public class UsuarioViewHolder  extends RecyclerView.ViewHolder {
    private final TextView nombreItemView;

    private UsuarioViewHolder(View itemView){
        super(itemView);
        nombreItemView = itemView.findViewById(R.id.textViewNombreUsuario);

    }

    public void bind(String nombre){ nombreItemView.setText(nombre); }

    static UsuarioViewHolder create(ViewGroup parent){
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.usuario_item, parent, false);
        return new UsuarioViewHolder(view);
    }
}
