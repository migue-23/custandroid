package com.example.cust2.entities;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity (indices = {@Index(value = {"scid", "nombreServicio", "nombreCliente"}, unique = true)})
public class ServicioCliente {

    @PrimaryKey(autoGenerate = true)
    public int scid;
    public String nombreServicio;
    public String nombreCliente;
    public String nombreUsuario;

    public int getScid() { return scid; }
    public void setScid(int scid) { this.scid = scid; }

    public String getNombreServicio() { return nombreServicio; }
    public void setNombreServicio(String nombreServicio) { this.nombreServicio = nombreServicio; }

    public String getNombreCliente() { return nombreCliente; }
    public void setNombreCliente(String nombreCliente) { this.nombreCliente = nombreCliente; }

    public String getNombreUsuario() { return nombreUsuario; }
    public void setNombreUsuario(String nombreUsuario) { this.nombreUsuario = nombreUsuario; }

    @Ignore
    public ServicioCliente() {
    }

    public ServicioCliente(int scid, String nombreServicio, String nombreCliente, String nombreUsuario) {
        this.scid = scid;
        this.nombreServicio = nombreServicio;
        this.nombreCliente = nombreCliente;
        this.nombreUsuario = nombreUsuario;
    }
}
